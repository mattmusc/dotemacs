;; -------~-------~--~------------------~------
;; EMACS CONFIG
;; matteo.muscella@gmail.com
;; -------~-------~--~------------------~------

(setq user-full-name "Matteo Muscella"
      user-mail-address "matteo.muscella@gmail.com")

;; set the PATH to be the same as my shells
(if (not (getenv "TERM_PROGRAM"))
    (setenv "PATH"
            (shell-command-to-string
             "source $HOME/.bashrc && printf $PATH")))
(setq exec-path (append exec-path '("/usr/texbin" "/usr/local/bin")))

(require 'package)
(add-to-list
 'package-archives
 '("melpa" . "http://melpa.org/packages/") t)
(when (< emacs-major-version 24)
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize)

(let ((default-directory "~/.emacs.d/elpa/"))
  (normal-top-level-add-subdirs-to-load-path))
(add-to-list 'load-path "~/.emacs.d/packages/")
(let ((default-directory "~/.emacs.d/packages/"))
  (normal-top-level-add-subdirs-to-load-path))
(add-to-list 'load-path "~/.emacs.d/themes/")

(setq frame-title-format #'("" "~ %b  ~")
      initial-scratch-message ";;"
      custom-theme-directory "~/.emacs.d/themes"
      linum-format "  %3d "
      transient-mark-mode 0
      inhibit-splash-screen t)

(when (display-graphic-p)
  (cond ((string-equal system-type "darwin")
         (progn
           (menu-bar-mode 1)
           (set-face-attribute
            'default nil
            :family "Anonymice Powerline" :height 140)
           (load-theme 'sanityinc-tomorrow-day t)
         ))
        ((or (string-equal system-type "gnu")
             (string-equal system-type "gnu/linux"))
         (progn
           (menu-bar-mode 0)
           (set-face-attribute 'default nil :family "Agave" :height 100)
           (load-theme 'sanityinc-tomorrow-night t)
           ))))

(tool-bar-mode 0)
(scroll-bar-mode 0)
(blink-cursor-mode 0)
(column-number-mode 1)
(size-indication-mode 1)
(set-fringe-mode nil)
(show-paren-mode t)

;; mode line
;; (setq-default
;;  mode-line-format
;;  `((:propertize " %b " face mode-line)
;;    (:propertize mode-line-modified face mode-line)
;;    (:propertize " %m" face mode-line)
;;    (:propertize " :: line %l,%c %p" face mode-line)
;;    ((type x w32 mac))
;;    ))

(global-font-lock-mode t)
(setq magit-auto-revert-mode nil)
(setq magit-last-seen-setup-instructions "1.4.0")

;; whitespace mode
(require 'whitespace)
(setq whitespace-style '(face trailing))
(global-whitespace-mode t)

;; backup files
(setq make-backup-files nil
      auto-save-default nil
      save-place-file "~/.temp/saved-places")

;; buffers
(iswitchb-mode 1)
(setq iswitchb-buffer-ignore '("^ " "*Buffer"))
(setq confirm-kill-emacs (quote y-or-n-p))
(defalias 'yes-or-no-p 'y-or-n-p)

;; ido
(defvar ido-cur-item nil)
(defvar ido-default-item nil)
(defvar ido-cur-list nil)
(setq-default ido-last-directory-list "~/.ido.last"
              ido-enable-flex-matching t
              ido-everywhere t)
(ido-mode 1)
(setq ido-file-extensions-order
      '(".org" ".txt" ".py" ".emacs" ".xml" ".el" ".ini" ".cfg"
        ".cnf"))

;; ispell
(setq-default ispell-personal-dictionary "~/.emacs.d/ispell_MyDictionary"
              ispell-program-name "ispell")
(setq ispell-dictionary "american"
      ispell-extra-args '()
      ispell-silently-savep t)

(require 'browse-kill-ring)
(require 'rainbow-mode)
(require 'rainbow-delimiters)

(put 'narrow-to-region 'disabled nil)
(put 'erase-buffer 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'downcase-region 'disabled nil)


;; Gnus -- Gmail

(require 'nnir)

;;@see http://www.emacswiki.org/emacs/GnusGmail#toc1
(setq gnus-select-method '(nntp "news.gmane.org"))

;; ask encyption password once
(setq epa-file-cache-passphrase-for-symmetric-encryption t)

(setq smtpmail-auth-credentials "~/.authinfo.gpg")

;;@see http://gnus.org/manual/gnus_397.html
(add-to-list 'gnus-secondary-select-methods
             '(nnimap "gmail"
                      (nnimap-address "imap.gmail.com")
                      (nnimap-server-port 993)
                      (nnimap-stream ssl)
                      (nnir-search-engine imap)
                      (nnimap-authinfo-file "~/.authinfo.gpg")
                      ; @see http://www.gnu.org/software/emacs/manual/html_node/gnus/Expiring-Mail.html
                      ;; press 'E' to expire email
                      (nnmail-expiry-target "nnimap+gmail:[Gmail]/Trash")
                      (nnmail-expiry-wait 90)
                      )
             )

(setq-default
  gnus-summary-line-format "%U%R%z %(%&user-date;  %-15,15f  %B%s%)\n"
  gnus-user-date-format-alist '((t . "%Y-%m-%d %H:%M"))
  gnus-summary-thread-gathering-function 'gnus-gather-threads-by-references
  gnus-sum-thread-tree-false-root ""
  gnus-sum-thread-tree-indent ""
  gnus-sum-thread-tree-leaf-with-other "-> "
  gnus-sum-thread-tree-root ""
  gnus-sum-thread-tree-single-leaf "|_ "
  gnus-sum-thread-tree-vertical "|")

(setq gnus-thread-sort-functions
      '(
        (not gnus-thread-sort-by-date)
        (not gnus-thread-sort-by-number)
        ))

; NO 'passive
(setq gnus-use-cache t)
(setq gnus-use-adaptive-scoring t)
(setq gnus-save-score t)
(add-hook 'mail-citation-hook 'sc-cite-original)
(add-hook 'message-sent-hook 'gnus-score-followup-article)
(add-hook 'message-sent-hook 'gnus-score-followup-thread)
; @see http://stackoverflow.com/questions/945419/how-dont-use-gnus-adaptive-scoring-in-some-newsgroups
(setq gnus-parameters
      '(("nnimap.*"
         (gnus-use-scoring nil))
        ))

(defvar gnus-default-adaptive-score-alist
  '((gnus-kill-file-mark (from -10))
    (gnus-unread-mark)
    (gnus-read-mark (from 10) (subject 30))
    (gnus-catchup-mark (subject -10))
    (gnus-killed-mark (from -1) (subject -30))
    (gnus-del-mark (from -2) (subject -15))
    (gnus-ticked-mark (from 10))
    (gnus-dormant-mark (from 5))))

(setq  gnus-score-find-score-files-function
       '(gnus-score-find-hierarchical gnus-score-find-bnews bbdb/gnus-score)
       )

;; BBDB: Address list

(require 'bbdb)
(bbdb-initialize 'message 'gnus 'sendmail)
(setq bbdb-file "~/bbdb.db")
(add-hook 'gnus-startup-hook 'bbdb-insinuate-gnus)
(setq bbdb/mail-auto-create-p t
      bbdb/news-auto-create-p t)
(defvar bbdb-time-internal-format "%Y-%m-%d"
  "The internal date format.")
  ;;;###autoload
(defun bbdb-timestamp-hook (record)
  "For use as a `bbdb-change-hook'; maintains a notes-field called `timestamp'
    for the given record which contains the time when it was last modified.  If
    there is such a field there already, it is changed, otherwise it is added."
  (bbdb-record-putprop record 'timestamp (format-time-string
                                          bbdb-time-internal-format
                                          (current-time))))

(add-hook 'message-mode-hook
          '(lambda ()
             (flyspell-mode t)
             (local-set-key "<TAB>" 'bbdb-complete-name)))

;; Fetch only part of the article if we can.  I saw this in someone
;; else's .gnus
(setq gnus-read-active-file 'some)

;; Tree view for groups.  I like the organisational feel this has.
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

;; Threads!  I hate reading un-threaded email -- especially mailing
;; lists.  This helps a ton!
(setq gnus-summary-thread-gathering-function
      'gnus-gather-threads-by-subject)

;; Also, I prefer to see only the top level message.  If a message has
;; several replies or is part of a thread, only show the first
;; message.  'gnus-thread-ignore-subject' will ignore the subject and
;; look at 'In-Reply-To:' and 'References:' headers.
(setq gnus-thread-hide-subtree t)
(setq gnus-thread-ignore-subject t)

; Personal Information
(setq user-full-name "Matteo Muscella"
      user-mail-address "matteo.muscella@gmail.com"
      ;message-generate-headers-first t
      )

;; Change email address for work folder.  This is one of the most
;; interesting features of Gnus.  I plan on adding custom .sigs soon
;; for different mailing lists.
;; Usage, FROM: My Name <work>
(setq gnus-posting-styles
      '((".*"
     (name "Matteo Muscella"
          (address "matteo.muscella@gmail.com"
                   (organization "")
                   (signature-file "~/.signature")
                   ("X-Troll" "Emacs is better than Vi")
                   )))))

; You need install the command line brower 'w3m' and Emacs plugin 'w3m'
(setq mm-text-html-renderer 'w3m)

(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
      smtpmail-auth-credentials '(("smtp.gmail.com" 587 "username@gmail.com" nil))
      smtpmail-default-smtp-server "smtp.gmail.com"
      smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587
      smtpmail-local-domain "homepc")
;http://www.gnu.org/software/emacs/manual/html_node/gnus/_005b9_002e2_005d.html
(setq gnus-use-correct-string-widths nil)

;; Projectile
(require 'grizzl)
(projectile-global-mode)
(setq projectile-enable-caching t)
(setq projectile-completion-system 'grizzl)
;; Press Command-p for fuzzy find in project
(global-set-key (kbd "s-p") 'projectile-find-file)
;; Press Command-b for fuzzy switch buffer
(global-set-key (kbd "s-b") 'projectile-switch-to-buffer)

;; Programming standards
(setq-default standard-indent 4
              tab-width 4
              indent-tabs-mode nil
              require-final-newline t
              c-default-style "ellemtel"
              c-basic-offset 4)

(defun my-java-mode-hook ()
  (setq c-basic-offset 4)
  (setq tab-width 4)
  (setq standard-indent 4))
(add-hook 'java-mode-hook 'my-java-mode-hook)

(setq-default compilation-window-height 10)
(add-hook 'java-mode-hook 'compile-mode-hook)
(add-hook 'c-mode-hook 'compile-mode-hook)
(add-hook 'c++-mode-hook 'compile-mode-hook)

;; cmake
(defun maybe-cmake-project-hook ()
  (if (file-exists-p "CMakeLists.txt") (cmake-project-mode)))
(add-hook 'c-mode-hook 'maybe-cmake-project-hook)
(add-hook 'c++-mode-hook 'maybe-cmake-project-hook)

;; python
(setq python-skeleton-autoinsert nil)
(setq-default python-indent-offset 4
              python-guess-indent nil)
(setq python-python-command "python3")
(setq python-shell-interpreter "python3 -i -c 'import sys sys.ps1=\'python> \''")

;; ruby


;; haskell
;; Make Emacs look in Cabal directory for binaries
(let ((my-cabal-path (expand-file-name "~/.cabal/bin")))
  (setenv "PATH" (concat my-cabal-path ":" (getenv "PATH")))
  (add-to-list 'exec-path my-cabal-path))

; HASKELL-MODE
; ------------

; Choose indentation mode
;; Use haskell-mode indentation
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;; Use hi2
;(require 'hi2)
;(add-hook 'haskell-mode-hook 'turn-on-hi2)
;; Use structured-haskell-mode
;(add-hook 'haskell-mode-hook 'structured-haskell-mode)

; Add F8 key combination for going to imports block
(eval-after-load 'haskell-mode
  '(define-key haskell-mode-map [f8] 'haskell-navigate-imports))

(custom-set-variables
 ; Set up hasktags (part 2)
 '(haskell-tags-on-save t)
 ; Set up interactive mode (part 2)
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-suggest-remove-import-lines t)
 ; Set interpreter to be "cabal repl"
 '(haskell-process-type 'cabal-repl))

; Add key combinations for interactive haskell-mode
(eval-after-load 'haskell-mode '(progn
  (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-or-reload)
  (define-key haskell-mode-map (kbd "C-c C-z") 'haskell-interactive-switch)
  (define-key haskell-mode-map (kbd "C-c C-n C-t") 'haskell-process-do-type)
  (define-key haskell-mode-map (kbd "C-c C-n C-i") 'haskell-process-do-info)
  (define-key haskell-mode-map (kbd "C-c C-n C-c") 'haskell-process-cabal-build)
  (define-key haskell-mode-map (kbd "C-c C-n c") 'haskell-process-cabal)
  (define-key haskell-mode-map (kbd "SPC") 'haskell-mode-contextual-space)))
(eval-after-load 'haskell-cabal '(progn
  (define-key haskell-cabal-mode-map (kbd "C-c C-z")'haskell-interactive-switch)
  (define-key haskell-cabal-mode-map (kbd "C-c C-k") 'haskell-interactive-mode-clear)
  (define-key haskell-cabal-mode-map (kbd "C-c C-c") 'haskell-process-cabal-build)
  (define-key haskell-cabal-mode-map (kbd "C-c c") 'haskell-process-cabal)))

(eval-after-load 'haskell-mode
  '(define-key haskell-mode-map (kbd "C-c C-o") 'haskell-compile))
(eval-after-load 'haskell-cabal
  '(define-key haskell-cabal-mode-map (kbd "C-c C-o") 'haskell-compile))

; GHC-MOD
; -------

(autoload 'ghc-init "ghc" nil t)
(autoload 'ghc-debug "ghc" nil t)
(add-hook 'haskell-mode-hook (lambda () (ghc-init)))

; COMPANY-GHC
; -----------

; Enable company-mode
(require 'company)
; Use company in Haskell buffers
; (add-hook 'haskell-mode-hook 'company-mode)
; Use company in all buffers
(add-hook 'after-init-hook 'global-company-mode)

;;(add-to-list 'company-backends 'company-ghc)
;;(custom-set-variables '(company-ghc-show-info t))

(add-hook 'haskell-mode-hook #'rainbow-delimiters-mode)

;; abbrevs
(setq abbrev-file-name
      "~/.emacs.d/abbrevs")
(setq save-abbrevs nil)
(if (file-exists-p abbrev-file-name)
    (quietly-read-abbrev-file))
(setq abbrev-mode nil)

;; two modes
(defvar default-mode (list "HTML" 'html-mode))
(defvar second-modes (list
                      (list "Erlang" "<erl>" "</erl>" 'erlang-mode)
                      (list "C++" "<?php" "?>" 'c++-mode)
                      (list "Python" "<?python" "?>" 'python-mode)
                      (list "Tcl" "<?" "?>" 'tcl-mode)
                      (list "Ruby" "<%" "%>" 'ruby-mode)
                      ))

;; auctex
(setq TeX-auto-save t
      TeX-PDF-mode t
      reftex-plug-into-AUCTeX t)
(add-hook 'LaTeX-mode-hook 'auto-fill-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(cond ((string-equal system-type "darwin")
       (setq TeX-view-program-list '(("Evince" "open %o")))))

;; markdown mode
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;; helm
(require 'helm)
(require 'helm-config)

;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t)

(helm-mode 1)
(eval-after-load "helm-regexp"
  '(setq helm-source-moccur
         (helm-make-source "Moccur"
             'helm-source-multi-occur :follow 1)))

;; shell mode
(require 'tramp)

(custom-set-variables
 '(tramp-default-method "ssh")          ; uses ControlMaster
 '(comint-scroll-to-bottom-on-input t)  ; always insert at the bottom
 '(comint-scroll-to-bottom-on-output nil) ; always add output at the bottom
 '(comint-scroll-show-maximum-output t) ; scroll to show max possible output
 ;; '(comint-completion-autolist t)     ; show completion list when ambiguous
 '(comint-input-ignoredups t)           ; no duplicates in command history
 '(comint-completion-addsuffix t)       ; insert space/slash after file completion
 '(comint-buffer-maximum-size 20000)    ; max length of the buffer in lines
 '(comint-prompt-read-only nil)         ; if this is t, it breaks shell-command
 '(comint-get-old-input (lambda () "")) ; what to run when i press enter on a
                                        ; line above the current prompt
 '(comint-input-ring-size 5000)         ; max shell history size
 '(protect-buffer-bury-p nil)
 '(comint-output-filter-functions)
 '(comint-strip-ctrl-m)
)
;; interpret and use ansi color codes in shell output windows
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;; keys
(global-set-key (kbd "C-x C-b") 'switch-to-buffer)
(global-set-key (kbd "C-c C-b") 'eval-buffer)
(global-set-key (kbd "C-c C-r") 'eval-region)
(global-set-key (kbd "C-c g") 'goto-line)
(global-set-key (kbd "C-c G") 'goto-char)
(global-set-key (kbd "C-c C-a")   'mark-whole-buffer)
(global-set-key (kbd "C-c y") 'browse-kill-ring)
(global-set-key (kbd "C-c o") 'occur)
(global-set-key (kbd "C-c e") 'erase-buffer)
(when window-system
  (global-unset-key (kbd "C-z")))
(global-set-key (kbd "C-c s") 'shell)
(global-set-key (kbd "C-c C-s") 'eshell)
(global-set-key (kbd "s-<left>") 'windmove-left)
(global-set-key (kbd "s-<right>") 'windmove-right)
(global-set-key (kbd "s-<up>") 'windmove-up)
(global-set-key (kbd "s-<down>") 'windmove-down)
(global-set-key (kbd "C-c n") 'linum-mode)
(global-set-key (kbd "C-c q") 'auto-fill-mode)
(global-set-key (kbd "s-=") 'text-scale-increase)
(global-set-key (kbd "s--") 'text-scale-decrease)
(global-set-key (kbd "s-F") 'clang-format-buffer)

;; custom functions
(defun eshell/clear ()
  "Clears the eshell buffer."
  (interactive)
  (let ((inhibit-read-only t))
    (erase-buffer)))
(setq eshell-prompt-function
      (lambda ()
        (concat
         (user-login-name)
         (if (= (user-uid) 0) " # " " $ "))))

(defun compile-mode-hook ()
  "Hook for changing keys for compiling. Rebinds 'C-c C-c' to compile command"
  (local-set-key (kbd "C-c C-c") 'compile))

(defun shorten-directory (dir max-length)
  "Show up to `max-length' characters of a directory name `dir'."
  (let ((path (reverse (split-string (abbreviate-file-name dir) "/")))
        (output ""))
    (when (and path (equal "" (car path)))
      (setq path (cdr path)))
    (while (and path (< (length output) (- max-length 4)))
      (setq output (concat (car path) "/" output))
      (setq path (cdr path)))
    (when path
      (setq output (concat ".../" output)))
    output))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(button ((t (:inherit link :underline t))))
 '(sh-quoted-exec ((t (:foreground "dark magenta"))) t))

;;
;; init.el ends here
